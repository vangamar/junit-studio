package launchcode;

public class BalancedBrackets {
    /**
     * The function BalancedBrackets should return true if an only if
     * the input string has a set of "balanced" brackets.
     *
     * That is, whether it consists entirely of pairs of opening/closing
     * brackets (in that order), none of which mis-nest. We consider a bracket
     * to be square-brackets: [ or ].
     *
     * The string may contain non-bracket characters as well.
     *
     * These strings have balanced brackets:
     *  "[LaunchCode]", "Launch[Code]", "[]LaunchCode", "", "[]"
     *
     * While these do not:
     *   "[LaunchCode", "Launch]Code[", "[", "]["
     *
     * @param str - to be validated
     * @return true if balanced, false otherwise
     */
    public static boolean hasBalancedBrackets(String str) {
        int brackets = 0;
        int openBrackPos = -1;
        int closeBrackPos = -1;
        int i;
        for (i=0; i<str.length(); i++) {
            if (str.charAt(i) == '[') {
                openBrackPos = i;
                break;
            }
        }
        for (i=0; i<str.length(); i++) {
            if (str.charAt(i) == ']') {
                closeBrackPos = i;
                break;
            }
        }

        if (openBrackPos == -1 || closeBrackPos == -1) {
            return false;
        }
        else if (openBrackPos >= closeBrackPos) {
            return false;
        }
        else {
            return true;
        }
//        for (char ch : str.toCharArray()) {
//            if (ch == '[') {
//                brackets++;
//            } else if (ch == ']') {
//                brackets--;
//            }
//        }
//        return brackets == 0;
    }
}
