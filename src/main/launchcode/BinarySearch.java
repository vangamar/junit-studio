package launchcode;

public class BinarySearch {

    /**
     * A binary search implementation for integer arrays.
     *
     * Info about binary search: https://www.geeksforgeeks.org/binary-search/
     *
     * @param sortedNumbers - must be sorted from least to greatest
     * @param n - number to search for
     * @return index of search item if it's found, -1 if not found
     */
    public static int binarySearch(int[] sortedNumbers, int x) {
        int r = sortedNumbers.length - 1;
        int l = 0;
        int[] arr = sortedNumbers;

//        while (right >= left) {
//            int mid = left + ((right - left) / 2);
//            if (sortedNumbers[mid] < n) {
//                left = mid;
//            } else if (sortedNumbers[mid] > n) {
//                right = mid;
//            } else if (sortedNumbers[left] == n) {
//                return left;
//            } else if (sortedNumbers[right]== n) {
//                return right;
//            } else {
//                return mid;
//            }
////            if (right == left) {
////                return -1;
////            }
        while (l <= r) {
            int m = l + (r - l) / 2;

            // Check if x is present at mid
            if (arr[m] == x)
                return m;

            // If x greater, ignore left half
            if (arr[m] < x)
                l = m + 1;

                // If x is smaller, ignore right half
            else
                r = m - 1;
        }

        return -1;
    }

}
