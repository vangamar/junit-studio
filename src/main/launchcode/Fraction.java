package launchcode;

/*
* Fraction class written by Brad Miller (interactive.org)
* */
public class Fraction implements Comparable<Fraction> {

    private Integer numerator;
    private Integer denominator;

    /** Creates a new instance of Fraction */
    public Fraction(Integer num, Integer den) {
        this.numerator = num;
        this.denominator = den;
    }

    public Fraction(Integer num) {
        this.numerator = num;
        this.denominator = 1;
    }

    public Integer getNumerator() {
        return numerator;
    }

    public void setNumerator(Integer numerator) {
        this.numerator = numerator;
    }

    public Integer getDenominator() {
        return denominator;
    }

    public void setDenominator(Integer denominator) {
        this.denominator = denominator;
    }


    public Fraction add(Fraction other) {
//        Integer newNum = other.getDenominator() * this.numerator + this.denominator * other.getNumerator();
//        Integer newDen = this.denominator * other.getDenominator();
//        Integer newNum;
//        Integer newDen;
        Integer den1 = this.denominator;
        Integer den2 = other.getDenominator();

        Integer gcd = Fraction.gcd(den1, den2);

        // Denominator of final fraction obtained
        // finding LCM of den1 and den2
        // LCM * GCD = a * b
        gcd = (den1*den2) / gcd;

        // Changing the fractions to have same denominator
        // Numerator of the final fraction obtained
        Integer num1 = this.numerator;
        Integer num2 = other.getNumerator();
        Integer num3 = (num1)*(gcd/den1) + (num2)*(gcd/den2);


        return new Fraction(num3, gcd);
    }

    public Fraction add(Integer other) {
        return add(new Fraction(other));
    }

    private static Integer gcd(Integer m, Integer n) {
        while (m % n != 0) {
            Integer oldm = m;
            Integer oldn = n;
            m = oldn;
            n = oldm % oldn;
        }
        return n;
    }


    public String toString() {
        return numerator.toString() + "/" + denominator.toString();
    }

    public boolean equals(Fraction other) {
        Integer num1 = this.numerator * other.getDenominator();
        Integer num2 = this.denominator * other.getNumerator();
        if (num1 == num2)
            return true;
        else
            return false;
    }

    public int compareTo(Fraction other) {
//        Integer num1 = this.numerator * other.getDenominator();
//        Integer num2 = this.denominator * other.getNumerator();
        double num1 = (double) this.numerator / (double) this.denominator;
        double num2 = (double) other.getNumerator() / (double) other.getDenominator();
        double comp = num1 - num2;
        if (num1 == num2) {
            return 0;
        }
        else if (num1 < num2) {
            return -1;
        }
        else {
            return 1;
        }
    }

}
        