package launchcode;

import java.util.TreeMap;

public class RomanNumeral {

//    enum Numeral {
//        I(1), V(5), X(10), L(50), C(100), D(500), M(1000);
//        int value;
//
//        Numeral(int value) {
//            this.value = value;
//        }
//    }
//
//    /**
//     * Convert positive integers to roman numerals
//     *
//     * Examples: 4 -> IV, 51 -> LI, 999 -> CMXCIX
//     *
//     * @param n - positive integer
//     * @return Roman numeral string version of integer
//     */
//    public static String fromInt (int n) {
//
//        if(n <= 0) {
//            throw new IllegalArgumentException();
//        }
//
//        StringBuilder buf = new StringBuilder();
//
//        final Numeral[] values = Numeral.values();
//        for (int i = values.length - 1; i >= 0; i--) {
//            while (n >= values[i].value) {
//                buf.append(values[i]);
//                n -= values[i].value;
//            }
//        }
//        return buf.toString();
//    }

    private final static TreeMap<Integer, String> map = new TreeMap<Integer, String>();

    static {

        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");

    }

    public final static String fromInt(int number) {
        int l =  map.floorKey(number);
        if ( number == l ) {
            return map.get(number);
        }
        return map.get(l) + fromInt(number-l);
    }
}
