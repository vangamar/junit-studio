package launchcode;

import org.junit.Test;

import static org.junit.Assert.*;

public class BalancedBracketsTest {

    //TODO: add tests here
    @Test
    public void testCorrectBracketDetections() {
        BalancedBrackets balBrack = new BalancedBrackets();

        assertEquals(true, BalancedBrackets.hasBalancedBrackets("words[]"));
        assertEquals(true, BalancedBrackets.hasBalancedBrackets("[words]"));
        assertEquals(false, BalancedBrackets.hasBalancedBrackets("]words["));
        assertEquals(false, BalancedBrackets.hasBalancedBrackets("[words"));
        assertEquals(false, BalancedBrackets.hasBalancedBrackets("words]"));
    }

    @Test
    public void testBinarySearch() {

        int[] arr = new int[]{1,2,3};

        assertEquals(0, BinarySearch.binarySearch(arr, 1));
        assertEquals(1, BinarySearch.binarySearch(arr, 2));
        assertEquals(2, BinarySearch.binarySearch(arr, 3));
        assertEquals(-1, BinarySearch.binarySearch(arr, 0));
        assertEquals(-1, BinarySearch.binarySearch(arr, -1));
        assertEquals(-1, BinarySearch.binarySearch(arr, 4));

    }

    @Test
    public void testRomanNumeral() {
        //     * Examples: 4 -> IV, 51 -> LI, 999 -> CMXCIX
        assertEquals("III", RomanNumeral.fromInt(3));
        assertEquals("IV", RomanNumeral.fromInt(4));
        assertEquals("LI", RomanNumeral.fromInt(51));
        assertEquals("CMXCIX", RomanNumeral.fromInt(999));

    }

    @Test
    public void testFraction() {

//        Fraction smallerFrac = new Fraction(2,4);
//        Fraction greaterFrac = new Fraction(3,4);
        Fraction smallerFrac = new Fraction(1,4);
        Fraction greaterFrac = new Fraction(5,4);

        boolean isEqual;
        isEqual = smallerFrac.equals(greaterFrac);
        assertEquals(false, isEqual );
        isEqual = greaterFrac.equals(greaterFrac);
        assertEquals(true, isEqual );
        isEqual = greaterFrac.equals(smallerFrac);
        assertEquals(false, isEqual );


        int compare;

        compare = smallerFrac.compareTo(greaterFrac) ;
        assertTrue(compare < 0);
        compare = greaterFrac.compareTo(smallerFrac) ;
        assertTrue(compare > 0);
        compare = smallerFrac.compareTo(smallerFrac) ;
        assertTrue(compare == 0);

        Fraction f1;
        Fraction f2;

        f1 = new Fraction(1,4);
        f2 = new Fraction(2,8);
        assertEquals("4/8", f1.add(f2).toString());


    }

}
